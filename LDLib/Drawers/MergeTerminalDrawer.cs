﻿
namespace LDLib
{
    internal class MergeTerminalDrawer
    {
        public static void Draw(MergeTerminalNode n, Graphics g, Rectangle rect, Graph graph, Font font, bool isActive)
        {
            var pen = MyPens.GetEnergyLine(isActive);

            g.DrawLine(pen, rect.X, rect.Y + rect.Height * 3 / 4, rect.X + rect.Width / 2, rect.Y + rect.Height * 3 / 4);
            g.DrawLine(pen, rect.X + rect.Width / 2, rect.Y, rect.X + rect.Width / 2, rect.Y + rect.Height * 3 / 4);

            if (graph.TryGetNode(n, Direction.Down, out var b))
            {
                g.DrawLine(pen, rect.X + rect.Width * 4 / 5, rect.Y + rect.Height * 3 / 4, rect.X + rect.Width * 4 / 5, rect.Y + rect.Height);
            }
        }
    }
}