﻿namespace LDLib
{
    internal class ContactDrawer
    {
        public static void Draw(ContactNode n, Graphics g, Rectangle rect, Graph graph, Font font, bool isActive)
        {
            var pen = MyPens.GetEnergyLine(isActive);
            int devide = 20;

            // left side horizontal line
            g.DrawLine(pen,
                rect.X,
                rect.Y + rect.Height * 3 / 4,
                rect.X + rect.Width / 2 - devide,
                rect.Y + rect.Height * 3 / 4);

            // right side horizontal line
            g.DrawLine(pen,
                rect.X + rect.Width / 2 + devide,
                rect.Y + rect.Height * 3 / 4,
                rect.X + rect.Width,
                rect.Y + rect.Height * 3 / 4);

            // left side virtical line
            g.DrawLine(pen,
                rect.X + rect.Width / 2 - devide,
                rect.Y + rect.Height / 2,
                rect.X + rect.Width / 2 - devide,
                rect.Y + rect.Height);

            // right side virtical line
            g.DrawLine(pen,
                rect.X + rect.Width / 2 + devide,
                rect.Y + rect.Height / 2,
                rect.X + rect.Width / 2 + devide,
                rect.Y + rect.Height);

            StringFormat stringFormat = new StringFormat();
            stringFormat.Alignment = StringAlignment.Center;
            stringFormat.LineAlignment = StringAlignment.Center;

            var r = new Rectangle(rect.Location, new Size(rect.Width, rect.Height / 2));
            g.DrawString(n.Operand, font, Brushes.Black, r, stringFormat);
        }
    }
}