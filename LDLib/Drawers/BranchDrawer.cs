﻿
namespace LDLib
{
    internal class BranchDrawer
    {
        public static void Draw(BranchNode n, Graphics g, Rectangle rect, Graph graph, Font font, bool isActive)
        {
            var pen = MyPens.GetEnergyLine(isActive);

            // left side horizontal line
            if (graph.TryGetNode(n, Direction.Left, out var left) && !LayoutNode.IsVacant(left))
            {
                g.DrawLine(pen,
                rect.X,
                rect.Y + rect.Height * 3 / 4,
                rect.X + rect.Width / 2,
                rect.Y + rect.Height * 3 / 4);
            }

            // right side horizontal line
            g.DrawLine(pen,
                rect.X + rect.Width / 2,
                rect.Y + rect.Height * 3 / 4,
                rect.X + rect.Width,
                rect.Y + rect.Height * 3 / 4);

            // up side vertical line
            if (graph.TryGetNode(n, Direction.Up, out var up) && !LayoutNode.IsVacant(up))
            {
                g.DrawLine(pen,
                    rect.X + rect.Width / 2,
                    rect.Y,
                    rect.X + rect.Width / 2,
                    rect.Y + rect.Height * 3 / 4);
            }

            // down side vertical line
            if (graph.TryGetNode(n, Direction.Down, out var down) && !LayoutNode.IsVacant(down))
            {
                g.DrawLine(pen,
                    rect.X + rect.Width / 2,
                    rect.Y + rect.Height * 3 / 4,
                    rect.X + rect.Width / 2,
                    rect.Y + rect.Height);
            }
        }
    }
}