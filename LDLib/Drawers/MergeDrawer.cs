﻿
namespace LDLib
{
    internal class MergeDrawer
    {
        public static void Draw(MergeNode n, Graphics g, Rectangle rect, Graph graph, Font font, bool isActive)
        {
            var pen = MyPens.GetEnergyLine(isActive);

            // left side horizontal line
            g.DrawLine(pen,
                rect.X,
                rect.Y + rect.Height * 3 / 4,
                rect.X + rect.Width / 2,
                rect.Y + rect.Height * 3 / 4);

            // right side horizontal line
            if (graph.TryGetNode(n, Direction.Right, out var right) && !LayoutNode.IsVacant(right))
            {
                g.DrawLine(pen,
                    rect.X + rect.Width / 2,
                    rect.Y + rect.Height * 3 / 4,
                    rect.X + rect.Width,
                    rect.Y + rect.Height * 3 / 4);
            }

            // up side vertical line
            if (graph.TryGetNode(n, Direction.Up, out var up) && !LayoutNode.IsVacant(up))
            {
                g.DrawLine(pen,
                    rect.X + rect.Width / 2,
                    rect.Y,
                    rect.X + rect.Width / 2,
                    rect.Y + rect.Height * 3 / 4);
            }

            // down side vertical line
            if (graph.TryGetNode(n, Direction.Down, out var down) && !LayoutNode.IsVacant(down))
            {
                g.DrawLine(pen,
                    rect.X + rect.Width / 2,
                    rect.Y + rect.Height * 3 / 4,
                    rect.X + rect.Width / 2,
                    rect.Y + rect.Height);
            }
        }
    }
}