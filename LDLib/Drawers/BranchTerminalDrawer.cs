﻿
namespace LDLib
{
    internal class BranchTerminalDrawer
    {
        public static void Draw(BranchTerminalNode n, Graphics g, Rectangle rect, Graph graph, Font font, bool isActive)
        {
            var pen = MyPens.GetEnergyLine(isActive);
            g.DrawLine(pen, rect.X + rect.Width / 2, rect.Y + rect.Height * 3 / 4, rect.X + rect.Width, rect.Y + rect.Height * 3 / 4);
            g.DrawLine(pen, rect.X + rect.Width / 2, rect.Y, rect.X + rect.Width / 2, rect.Y + rect.Height * 3 / 4);

            if (graph.TryGetNode(n, Direction.Down, out var b))
            {
                g.DrawLine(pen, rect.X + rect.Width / 5, rect.Y + rect.Height * 3 / 4, rect.X + rect.Width / 5, rect.Y + rect.Height);
            }
        }
    }
}