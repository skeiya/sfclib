﻿namespace LDLib
{
    internal class EarthDrawer
    {
        public static void Draw(EarthNode n, Graphics g, Rectangle rect, Graph graph, Font font, bool isActive)
        {
            var pen = MyPens.GetEnergyLine(isActive);
            g.DrawLine(pen, rect.X + rect.Width / 2, rect.Y, rect.X + rect.Width / 2, rect.Y + rect.Height);
            g.DrawLine(pen, rect.X, rect.Y + 75, rect.X + rect.Width / 2, rect.Y + 75);
        }
    }
}