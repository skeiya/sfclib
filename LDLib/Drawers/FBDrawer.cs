﻿namespace LDLib
{
    internal class FBDrawer
    {

        public static void Draw(FbNode n, Graphics g, Rectangle rect, Graph graph, Font font, bool isActive)
        {
            var pen = MyPens.GetEnergyLine(isActive);
            var fbRect = rect;
            fbRect.Inflate(-10, -10);
            g.DrawRectangle(pen, fbRect);

            g.DrawLine(pen, rect.X, rect.Y + 75, rect.X + 10, rect.Y + 75);
            g.DrawLine(pen, rect.X + rect.Width - 10, rect.Y + 75, rect.X + rect.Width, rect.Y + 75);
        }
    }
}