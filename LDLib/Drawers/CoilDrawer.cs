﻿
namespace LDLib
{
    internal class CoilDrawer
    {
        public static void Draw(CoilNode n, Graphics g, Rectangle rect, Graph graph, Font font, bool isActive)
        {
            int radius = 20;
            var pen = MyPens.GetEnergyLine(isActive);

            // left side horizontal line
            g.DrawLine(pen,
                rect.X,
                rect.Y + rect.Height * 3 / 4,
                rect.X + rect.Width / 2 - radius,
                rect.Y + rect.Height * 3 / 4);

            // right side horizontal line
            g.DrawLine(pen,
                rect.X + rect.Width / 2 + radius,
                rect.Y + rect.Height * 3 / 4,
                rect.X + rect.Width,
                rect.Y + rect.Height * 3 / 4);

            // center circle
            g.DrawEllipse(pen,
                rect.X + rect.Width / 2 - radius,
                rect.Y + rect.Height * 3 / 4 - radius,
                radius * 2,
                radius * 2);

            StringFormat stringFormat = new StringFormat();
            stringFormat.Alignment = StringAlignment.Center;
            stringFormat.LineAlignment = StringAlignment.Center;

            var r = new Rectangle(rect.Location, new Size(rect.Width, rect.Height / 2));
            g.DrawString(n.Operand, font, Brushes.Black, r, stringFormat);
        }
    }
}