﻿namespace LDLib
{
    internal class MyPens
    {
        private static Pen ActiveLine = new Pen(Brushes.LightGreen, 4);

        internal static Pen GetEnergyLine(bool isActive)
        {
            return isActive ? ActiveLine : Pens.Black;
        }
    }
}