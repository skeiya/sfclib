﻿namespace LDLib
{
    internal class MonitorService
    {
        private bool _isMonMode = false;

        internal event EventHandler<EventArgs>? Changed;

        internal bool IsMonMode()
        {
            return _isMonMode;
        }

        internal bool IsOnVariable(string variable)
        {
            if ("aaa".Equals(variable)) return true;
            if ("bbb".Equals(variable)) return true;
            return false;
        }

        internal void SetMonMode(bool value)
        {
            _isMonMode = value;
            Changed?.Invoke(this, EventArgs.Empty);
        }
    }
}