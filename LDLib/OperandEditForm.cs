﻿namespace LDLib
{
    public partial class OperandEditForm : Form
    {
        public OperandEditForm(string before)
        {
            InitializeComponent();
            this.StartPosition = FormStartPosition.Manual;
            textBox1.Text = before;
        }

        protected override bool ProcessCmdKey(ref Message msg, Keys keyData)
        {
            if (keyData == Keys.Enter)
            {
                this.DialogResult = DialogResult.OK;
                this.Close();
            }
            else if (keyData == Keys.Escape)
            {
                this.Close();
            }
            return base.ProcessCmdKey(ref msg, keyData);
        }

        internal string Operand => textBox1.Text;
    }
}
