﻿namespace LDLib
{
    public static class LayoutNode
    {
        internal static ILDNode Vacant = new VacantNode();

        internal static bool IsVacant(ILDNode n) => n.Type == BaseNodeType.Vacant;

        internal static bool IsMerge(ILDNode n) => n.Type == BaseNodeType.Merge;

        internal static bool IsMergeTerminal(ILDNode n) => n.Type == BaseNodeType.MergeTerminal;

        internal static bool IsBranch(ILDNode n) => n.Type == BaseNodeType.Branch;
        internal static bool IsBranchTerminal(ILDNode n) => n.Type == BaseNodeType.BranchTerminal;

        internal static bool IsEnd(ILDNode n) => n.Type == BaseNodeType.End;

        internal static bool IsNormal(ILDNode n) => n.Type == BaseNodeType.Normal || n.Type == BaseNodeType.NormalRightPreffer;

        internal static bool IsRightPreffer(ILDNode n) => n.Type == BaseNodeType.NormalRightPreffer;
    }
}
