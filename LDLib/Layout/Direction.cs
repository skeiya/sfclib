﻿namespace LDLib
{
    public enum Direction
    {
        Up,
        Down,
        Left,
        Right,
    }
}
