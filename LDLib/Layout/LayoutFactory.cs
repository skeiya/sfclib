﻿namespace LDLib
{
    public class LayoutFactory
    {
        internal static Layout Create(Graph graph, int min, MonitorService monitorService)
        {
            var locations = new Dictionary<ILDNode, Point>();
            var curLocation = new Point(0, 0);

            var peekX = 0;
            var peekY = 0;

            var dir = Direction.Right;
            var cur = graph.Root;
            var endCapture = LayoutNode.Vacant;
            while (true)
            {
                locations[cur] = curLocation;
                peekX = Math.Max(peekX, curLocation.X + cur.Size.Width);
                peekY = Math.Max(peekY, curLocation.Y + cur.Size.Height);
                foreach (var d in GetPriortyDirections(dir))
                {
                    if (!graph.TryGetNode(cur, d, out var next)) continue;

                    if (d == Direction.Left && LayoutNode.IsMerge(cur)) // つながっていないものと扱う
                    {
                        continue;
                    }
                    if (d == Direction.Right && LayoutNode.IsMerge(next)) // つながっていないものと扱う　ただし、Yは決まる
                    {
                        locations[next] = new Point(0/*dumy*/, curLocation.Y);
                        peekY = curLocation.Y + cur.Size.Height; // これがコンパクトなレイアウトのコツ。分岐のオフセットはマージからの帰り道のピークを使う。
                        continue;
                    }

                    if (d == Direction.Left) // 左は確定済みなので、そこに戻す。
                    {
                        curLocation = locations[next];
                    }
                    else if (d == Direction.Down)
                    {
                        if (locations.TryGetValue(next, out _)) // 戻り先があれば、確定済みなので、そこに戻す
                        {
                            curLocation = locations[next];
                        }
                        else
                        {
                            curLocation = new Point(curLocation.X, peekY); // 初めて下る場合は、peekYまで進める
                        }
                    }
                    else if (d == Direction.Right)
                    {
                        if (LayoutNode.IsMergeTerminal(next))
                        {
                            curLocation = new Point(peekX, curLocation.Y);
                        }
                        else if (LayoutNode.IsEnd(next))
                        {
                            curLocation = new Point(Math.Max(peekX, min - next.Size.Width), curLocation.Y);
                            endCapture = next;
                        }
                        else
                        {
                            curLocation = new Point(curLocation.X + cur.Size.Width, curLocation.Y);
                        }
                    }
                    else if (d == Direction.Up)
                    {
                        if (LayoutNode.IsMerge(next))
                        {
                            curLocation = new Point(curLocation.X, locations[next].Y); // Mergeに上がる場合のYは確定済み
                        }
                        else
                        {
                            curLocation = locations[next];
                        }
                    }

                    dir = d;
                    cur = next;
                    if (cur == graph.Root)
                    {
                        if (graph.TryGetNode(endCapture, Direction.Left, out var leftOfEnd) && LayoutNode.IsRightPreffer(leftOfEnd))
                        {
                            var org = locations[endCapture];
                            locations[leftOfEnd] = new Point(org.X - leftOfEnd.Size.Width, org.Y);
                        }
                        return new Layout(locations);
                    }
                    break;
                }
            }
        }


        private static IEnumerable<Direction> GetPriortyDirections(Direction direction)
        {
            var result = direction switch
            {
                Direction.Down => new List<Direction>() { Direction.Right, Direction.Down, Direction.Left, Direction.Up },
                Direction.Up => new List<Direction>() { Direction.Left, Direction.Up, Direction.Right, Direction.Down },
                Direction.Left => new List<Direction>() { Direction.Down, Direction.Left, Direction.Up, Direction.Right },
                Direction.Right => new List<Direction>() { Direction.Up, Direction.Right, Direction.Down, Direction.Left },
                _ => throw new NotImplementedException(),
            };
            return result;
        }

    }
}
