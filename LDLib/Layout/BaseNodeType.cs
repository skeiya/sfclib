﻿namespace LDLib
{
    public enum BaseNodeType
    {
        Normal,
        NormalRightPreffer,
        Branch,
        BranchTerminal,
        Merge,
        MergeTerminal,
        Start,
        End,
        Vacant,
    }
}
