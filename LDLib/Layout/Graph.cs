﻿using System.Diagnostics.CodeAnalysis;

namespace LDLib
{
    public class Graph
    {
        private readonly ILDNode _root;
        private Connection _connection = new();

        internal ILDNode Root => _root;

        internal Graph(ILDNode root)
        {
            _root = root;
        }

        internal void InterConnect(ILDNode src, Direction dir, ILDNode dst)
        {
            _connection.InterConnect(src, dir, dst);
        }

        internal void Insert(ILDNode src, Direction dir, ILDNode dst)
        {
            _connection.Insert(src, dir, dst);
        }

        internal bool TryGetNode(ILDNode key, Direction dir, [MaybeNullWhen(false)] out ILDNode value)
        {
            if (!_connection.TryGetNode(key, dir, out value)) return false;
            return !LayoutNode.IsVacant(value);
        }

        internal ILDNode Remove(ILDNode n)
        {
            return _connection.Remove(n);
        }
    }
}
