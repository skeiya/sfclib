﻿namespace LDLib
{
    // Immutable info
    public class Layout
    {
        private readonly Dictionary<ILDNode, Point> _locations;

        internal Layout(Dictionary<ILDNode, Point> locations)
        {
            this._locations = locations;
        }

        internal bool TryGetNodeRect(ILDNode n, out Rectangle rect)
        {
            if (n == null)
            {
                rect = Rectangle.Empty;
                return false;
            }
            if (!_locations.TryGetValue(n, out var tmp))
            {
                rect = default;
                return false;
            }
            rect = new Rectangle(tmp, n.Size);
            return true;
        }

        internal IEnumerable<ILDNode> AllNodes
        {
            get { return _locations.Keys; }
        }
    }
}
