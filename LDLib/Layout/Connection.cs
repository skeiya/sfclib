﻿using System.Diagnostics.CodeAnalysis;

namespace LDLib
{
    public class Connection
    {
        // src -> dir -> dst
        private Dictionary<ILDNode, Dictionary<Direction, ILDNode>> _core = new();

        internal void InterConnect(ILDNode src, Direction dir, ILDNode dst)
        {
            Connect(src, dir, dst);
            Connect(dst, Reverse(dir), src);
        }

        private void Connect(ILDNode src, Direction dir, ILDNode dst)
        {
            if (!_core.TryGetValue(src, out _))
            {
                _core.Add(src, new Dictionary<Direction, ILDNode>());
            }
            _core[src][dir] = dst;
        }

        private Direction Reverse(Direction dir)
        {
            switch (dir)
            {
                case Direction.Left: return Direction.Right;
                case Direction.Right: return Direction.Left;
                case Direction.Up: return Direction.Down;
                case Direction.Down: return Direction.Up;
            }
            throw new NotSupportedException();
        }

        internal ILDNode Remove(ILDNode n)
        {
            if (LayoutNode.IsNormal(n))
            {
                if (TryGetNode(n, Direction.Right, out var right) && LayoutNode.IsMergeTerminal(right))
                {
                    DeleteAtomicMergeTerminal(right);
                }
                if (!TryGetNode(n, Direction.Left, out var left) || !LayoutNode.IsBranchTerminal(left))
                {
                    return DeleteAtomicNomal(n);
                }
                DeleteAtomicNomal(n);
                return DeleteAtomicBranchTerminal(left);
            }
            if (LayoutNode.IsMergeTerminal(n))
            {
                return DeleteAtomicMergeTerminal(n);
            }
            if (LayoutNode.IsBranchTerminal(n))
            {
                return DeleteAtomicBranchTerminal(n);
            }
            if (LayoutNode.IsMerge(n))
            {
                return DeleteAtomicMerge(n);
            }
            if (LayoutNode.IsBranch(n))
            {
                return DeleteAtomicBranch(n);
            }
            return n;
        }

        private ILDNode DeleteAtomicBranch(ILDNode n)
        {
            if (!TryGetNode(n, Direction.Up, out var up) || LayoutNode.IsVacant(up))
            {
                return n;
            }
            else
            {
                if (TryGetNode(n, Direction.Right, out var right) && !LayoutNode.IsVacant(right)) return n;
                if (!TryGetNode(n, Direction.Down, out var down)) return n;
                InterConnect(up, Direction.Down, down);
                _core.Remove(n);
                return up;
            }
        }

        private ILDNode DeleteAtomicMerge(ILDNode n)
        {
            if (!TryGetNode(n, Direction.Up, out var up) || LayoutNode.IsVacant(up))
            {
                return n;
            }
            else
            {
                if (!TryGetNode(n, Direction.Left, out var left)) return n;
                if (!TryGetNode(n, Direction.Down, out var down)) return n;
                InterConnect(left, Direction.Right, LayoutNode.Vacant);
                InterConnect(up, Direction.Down, down);
                _core.Remove(n);
                return up;
            }
        }

        private ILDNode DeleteAtomicBranchTerminal(ILDNode n)
        {
            if (TryGetNode(n, Direction.Right, out var right) && !LayoutNode.IsVacant(right)) return n;
            if (!TryGetNode(n, Direction.Up, out var up) || LayoutNode.IsVacant(up)) return n;

            if (!TryGetNode(up, Direction.Up, out var upUp) || LayoutNode.IsVacant(upUp))
            {
                if (!TryGetNode(up, Direction.Left, out var upLeft)) return n;
                if (!TryGetNode(up, Direction.Right, out var upRight)) return n;
                InterConnect(upLeft, Direction.Right, upRight);
                _core.Remove(n);
                _core.Remove(up);
                return upLeft;
            }
            else
            {
                InterConnect(upUp, Direction.Down, n);
                if (!TryGetNode(up, Direction.Right, out var upRight)) return n;
                InterConnect(n, Direction.Right, upRight);
                _core.Remove(up);
                return n;
            }
        }

        private ILDNode DeleteAtomicMergeTerminal(ILDNode n)
        {
            if (!TryGetNode(n, Direction.Up, out var up)) return n;
            if (!TryGetNode(up, Direction.Up, out var upUp) || LayoutNode.IsVacant(upUp))
            {
                if (!TryGetNode(n, Direction.Left, out var left)) return n;
                InterConnect(left, Direction.Right, LayoutNode.Vacant);

                if (!TryGetNode(up, Direction.Left, out var upLeft)) return n;
                if (!TryGetNode(up, Direction.Right, out var upRight)) return n;
                InterConnect(upLeft, Direction.Right, upRight);
                _core.Remove(n);
                _core.Remove(up);
                return left;
            }
            else
            {
                if (!TryGetNode(n, Direction.Left, out var left)) return n;
                InterConnect(left, Direction.Right, LayoutNode.Vacant);
                if (!TryGetNode(up, Direction.Left, out var upLeft)) return n;
                InterConnect(upLeft, Direction.Right, n);
                InterConnect(upUp, Direction.Down, n);
                _core.Remove(up);
                return left;

            }
        }

        private ILDNode DeleteAtomicNomal(ILDNode n)
        {
            if (!TryGetNode(n, Direction.Left, out var left) || LayoutNode.IsVacant(left)) return n;
            if (!TryGetNode(n, Direction.Right, out var right)) return n;
            InterConnect(left, Direction.Right, right);
            _core.Remove(n);
            return left;
        }

        internal void Insert(ILDNode src, Direction dir, ILDNode target)
        {
            if (!TryGetNode(src, dir, out var dst))
            {
                InterConnect(src, dir, target);
            }
            else
            {
                InterConnect(src, dir, target);
                InterConnect(target, dir, dst);
            }
        }

        internal bool TryGetNode(ILDNode key, Direction dir, [MaybeNullWhen(false)] out ILDNode value)
        {
            if (!_core.TryGetValue(key, out var dirDic))
            {
                value = null;
                return false;
            }
            return dirDic.TryGetValue(dir, out value);
        }
    }
}