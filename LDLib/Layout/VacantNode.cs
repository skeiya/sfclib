﻿namespace LDLib
{
    internal class VacantNode : ILDNode
    {
        internal VacantNode()
        {
        }

        public BaseNodeType Type => BaseNodeType.Vacant;

        public Size Size => throw new NotImplementedException();

        public string ActiveRepresentativeVariable => throw new NotImplementedException();

        public void Draw(Graphics g, Rectangle rect, Graph graph, Font font, bool isActive)
        {
            throw new NotImplementedException();
        }
    }
}
