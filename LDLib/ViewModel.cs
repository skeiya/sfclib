﻿namespace LDLib
{
    internal class ViewModel
    {
        public ViewModel(Layout layout)
        {
            _layout = layout;
        }

        internal event EventHandler<EventArgs>? Changed;

        private ILDNode _selected = LayoutNode.Vacant;
        public ILDNode Selected
        {
            get
            {
                return _selected; ;
            }
            set
            {
                if (_selected.Equals(value)) return;
                Changed?.Invoke(this, new EventArgs());
                _selected = value;
            }
        }


        private Layout _layout { get; set; }
        public Layout Layout
        {
            get { return _layout; }
            set
            {
                Changed?.Invoke(this, new EventArgs());
                _layout = value;
            }
        }

        public IEnumerable<ILDNode> AllNodes => Layout.AllNodes;


        internal bool TryGetNodeRect(ILDNode n, out Rectangle rect)
        {
            return Layout.TryGetNodeRect(n, out rect);
        }
    }
}
