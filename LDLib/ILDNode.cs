﻿namespace LDLib
{
    internal interface ILDNode
    {
        void Draw(Graphics g, Rectangle rect, Graph graph, Font font, bool isActive);
        BaseNodeType Type { get; }
        Size Size { get; }
        string ActiveRepresentativeVariable { get; }
    }
}