﻿namespace LDLib
{
    internal class EnergySrcNode : ILDNode
    {

        public EnergySrcNode()
        {
        }

        public BaseNodeType Type => BaseNodeType.Start;

        public Size Size => new Size(25, 100);

        public string ActiveRepresentativeVariable => throw new NotImplementedException();

        public void Draw(Graphics g, Rectangle rect, Graph graph, Font font, bool isActive)
        {
            EnergySrcDrawer.Draw(this, g, rect, graph, font, isActive);
        }
    }
}