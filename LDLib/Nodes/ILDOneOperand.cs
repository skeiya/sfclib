﻿namespace LDLib
{
    internal interface ILDOneOperand : ILDNode
    {
        public string Operand { get; internal set; }
    }
}