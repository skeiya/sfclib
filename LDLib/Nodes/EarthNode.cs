﻿namespace LDLib
{
    internal class EarthNode : ILDNode
    {
        public EarthNode()
        {
        }

        public BaseNodeType Type => BaseNodeType.End;

        public Size Size => new Size(25, 100);

        public string ActiveRepresentativeVariable => throw new NotImplementedException();

        public void Draw(Graphics g, Rectangle rect, Graph graph, Font font, bool isActive)
        {
            EarthDrawer.Draw(this, g, rect, graph, font, isActive);
        }
    }
}