﻿namespace LDLib
{
    internal class MergeTerminalNode : ILDNode
    {
        public MergeTerminalNode()
        {
        }

        public BaseNodeType Type => BaseNodeType.MergeTerminal;

        public Size Size => new Size(25, 100);

        public string ActiveRepresentativeVariable => throw new NotImplementedException();

        public void Draw(Graphics g, Rectangle rect, Graph graph, Font font, bool isActive)
        {
            MergeTerminalDrawer.Draw(this, g, rect, graph, font, isActive);
        }
    }
}