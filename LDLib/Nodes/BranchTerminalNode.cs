﻿namespace LDLib
{
    internal class BranchTerminalNode : ILDNode
    {
        public BranchTerminalNode()
        {
        }

        public BaseNodeType Type => BaseNodeType.BranchTerminal;

        public Size Size => new Size(25, 100);

        public string ActiveRepresentativeVariable => throw new NotImplementedException();

        public void Draw(Graphics g, Rectangle rect, Graph graph, Font font, bool isActive)
        {
            BranchTerminalDrawer.Draw(this, g, rect, graph, font, isActive);
        }
    }
}