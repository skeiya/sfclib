﻿namespace LDLib
{
    internal class BranchNode : ILDNode
    {
        public BranchNode()
        {
        }

        public BaseNodeType Type => BaseNodeType.Branch;

        public Size Size => new Size(25, 100);

        public string ActiveRepresentativeVariable => throw new NotImplementedException();

        public void Draw(Graphics g, Rectangle rect, Graph graph, Font font, bool isActive)
        {
            BranchDrawer.Draw(this, g, rect, graph, font, isActive);
        }
    }
}