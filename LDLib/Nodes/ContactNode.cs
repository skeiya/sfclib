﻿namespace LDLib
{
    internal class ContactNode : ILDOneOperand
    {
        private Func<string, int> _widthGetter;
        private const int _minWidth = 200;
        private Size _size = new Size(_minWidth, 100);

        public ContactNode(Func<string, int> widthGetter)
        {
            _widthGetter = widthGetter;
        }

        private string _operand = string.Empty;
        public string Operand
        {
            set
            {
                _operand = value;
                _size = new Size(Math.Max(_minWidth, _widthGetter(_operand)), _size.Height);
            }
            get
            {
                return _operand;
            }
        }

        public BaseNodeType Type => BaseNodeType.Normal;

        public Size Size => _size;

        public string ActiveRepresentativeVariable => _operand;

        public void Draw(Graphics g, Rectangle rect, Graph graph, Font font, bool isActive)
        {
            ContactDrawer.Draw(this, g, rect, graph, font, isActive);
        }
    }
}