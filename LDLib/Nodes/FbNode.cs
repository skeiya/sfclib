﻿namespace LDLib
{
    internal class FbNode : ILDNode
    {
        public FbNode()
        {
        }

        public BaseNodeType Type => BaseNodeType.NormalRightPreffer;

        public Size Size => new Size(200, 500);

        public string ActiveRepresentativeVariable => "fb_eno";

        public void Draw(Graphics g, Rectangle rect, Graph graph, Font font, bool isActive)
        {
            FBDrawer.Draw(this, g, rect, graph, font, isActive);
        }
    }
}