﻿namespace LDLib
{
    internal class MergeNode : ILDNode
    {

        public MergeNode()
        {
        }

        public BaseNodeType Type => BaseNodeType.Merge;

        public Size Size => new Size(25, 100);

        public string ActiveRepresentativeVariable => throw new NotImplementedException();

        public void Draw(Graphics g, Rectangle rect, Graph graph, Font font, bool isActive)
        {
            MergeDrawer.Draw(this, g, rect, graph, font, isActive);
        }
    }
}