﻿namespace LDLib
{
    internal class DrawService
    {
        internal static void Draw(Graphics graphics, ViewModel viewModel, Graph graph, Font font, MonitorService monitorService)
        {
            var layout = viewModel.Layout;
            var activeNodes = GetActiveMap(layout.AllNodes, graph, monitorService);
            foreach (var n in layout.AllNodes)
            {
                if (!layout.TryGetNodeRect(n, out var rect)) return;
                n.Draw(graphics, rect, graph, font, activeNodes[n]);

                if (graph.TryGetNode(n, Direction.Right, out var r))
                {
                    if (!layout.TryGetNodeRect(r, out var dst)) return;
                    var pen = MyPens.GetEnergyLine(activeNodes[n]);
                    graphics.DrawLine(pen, rect.Left + rect.Width, rect.Top + 75, dst.Left, dst.Top + 75);
                }
                if (graph.TryGetNode(n, Direction.Down, out var b))
                {
                    if (!layout.TryGetNodeRect(b, out var dst)) return;
                    var pen = MyPens.GetEnergyLine(activeNodes[n]);
                    graphics.DrawLine(pen, rect.Left + rect.Width / 2, rect.Bottom, dst.Left + dst.Width / 2, dst.Top);
                }
            }

            if (layout.TryGetNodeRect(viewModel.Selected, out var selectedRect))
            {
                selectedRect.Inflate(-1, -1);
                graphics.DrawRectangle(Pens.Green, selectedRect);
            }
        }

        private static Dictionary<ILDNode, bool> GetActiveMap(IEnumerable<ILDNode> allNodes, Graph graph, MonitorService monitorService)
        {
            var result = new Dictionary<ILDNode, bool>();
            foreach (var n in allNodes)
            {
                result.Add(n, IsActive(n, monitorService, result, graph));
            }
            return result;
        }

        private static bool IsActive(ILDNode n, MonitorService monitorService, Dictionary<ILDNode, bool> dic, Graph graph)
        {
            if (!monitorService.IsMonMode()) return false;
            if (dic.TryGetValue(n, out bool active)) return active;
            if (n.Type == BaseNodeType.Start) return true;
            if (n.Type == BaseNodeType.Normal || n.Type == BaseNodeType.NormalRightPreffer)
            {
                if (!monitorService.IsOnVariable(n.ActiveRepresentativeVariable)) return false;
                if (graph.TryGetNode(n, Direction.Left, out var left) && IsActive(left, monitorService, dic, graph)) return true;
                return false;
            }
            if (n.Type == BaseNodeType.Branch || n.Type == BaseNodeType.BranchTerminal)
            {
                if (graph.TryGetNode(n, Direction.Left, out var left) && IsActive(left, monitorService, dic, graph)) return true;
                if (graph.TryGetNode(n, Direction.Up, out var up) && IsActive(up, monitorService, dic, graph)) return true;
                return false;
            }
            if (n.Type == BaseNodeType.Merge || n.Type == BaseNodeType.MergeTerminal)
            {
                if (graph.TryGetNode(n, Direction.Left, out var left) && IsActive(left, monitorService, dic, graph)) return true;
                if (graph.TryGetNode(n, Direction.Down, out var down) && IsActive(down, monitorService, dic, graph)) return true;
                return false;
            }
            if (n.Type == BaseNodeType.End)
            {
                if (graph.TryGetNode(n, Direction.Left, out var left) && IsActive(left, monitorService, dic, graph)) return true;
                return false;
            }
            return false;
        }
    }
}