﻿namespace LDLib
{
    internal class EditService
    {
        private Graph _graph;
        private Control _parent;

        public EditService(Graph graph, Control parent)
        {
            this._graph = graph;
            this._parent = parent;
        }

        internal event EventHandler<EventArgs>? ModelChanged;

        internal static Graph CreateGraph()
        {
            var enSrc = new EnergySrcNode();
            var graph = new Graph(enSrc);
            var earth = new EarthNode();
            graph.Insert(enSrc, Direction.Right, earth);
            return graph;
        }

        internal ILDNode InsertFb(ILDNode selected)
        {
            if (LayoutNode.IsVacant(selected)) return selected;
            var fb = new FbNode();
            _graph.Insert(selected, Direction.Right, fb);
            ModelChanged?.Invoke(this, EventArgs.Empty);
            return fb;
        }

        internal void InsertContact(ViewModel viewModel)
        {
            if (LayoutNode.IsVacant(viewModel.Selected)) return;
            var contact = new ContactNode(WidthGetter);
            _graph.Insert(viewModel.Selected, Direction.Right, contact);
            ModelChanged?.Invoke(this, EventArgs.Empty);
            viewModel.Selected = contact;

            EditOperand(viewModel, contact);
        }

        internal void Edit(ViewModel viewModel)
        {
            if (LayoutNode.IsVacant(viewModel.Selected)) return;
            var n = viewModel.Selected as ILDOneOperand;
            if (n == null) return;
            EditOperand(viewModel, n);
        }

        internal void Delete(ViewModel viewModel)
        {
            if (LayoutNode.IsVacant(viewModel.Selected)) return;
            viewModel.Selected = _graph.Remove(viewModel.Selected);
            ModelChanged?.Invoke(this, EventArgs.Empty);
        }

        internal void InsertCoil(ViewModel viewModel)
        {
            if (LayoutNode.IsVacant(viewModel.Selected)) return;
            var coil = new CoilNode(WidthGetter);
            _graph.Insert(viewModel.Selected, Direction.Right, coil);
            ModelChanged?.Invoke(this, EventArgs.Empty);
            viewModel.Selected = coil;

            EditOperand(viewModel, coil);
        }

        private void EditOperand(ViewModel viewModel, ILDOneOperand node)
        {
            using (var f = new OperandEditForm(node.Operand))
            {
                if (!viewModel.Layout.TryGetNodeRect(node, out var rect)) return;
                f.Location = _parent.PointToScreen(new Point(rect.X + (rect.Width - f.Width) / 2, rect.Y));
                if (f.ShowDialog(_parent) != DialogResult.OK) return;
                node.Operand = f.Operand;
                ModelChanged?.Invoke(this, EventArgs.Empty);
            }
        }

        internal void InsertOrContact(ViewModel viewModel)
        {
            var orgSel = viewModel.Selected;
            if (LayoutNode.IsVacant(orgSel)) return;

            if (!_graph.TryGetNode(orgSel, Direction.Left, out var left) || LayoutNode.IsVacant(left)) return;
            if (!_graph.TryGetNode(orgSel, Direction.Right, out var right) || LayoutNode.IsVacant(right)) return;

            var contact = new ContactNode(WidthGetter);
            var br = new BranchNode();
            var mg = new MergeNode();

            if (LayoutNode.IsBranch(left) && LayoutNode.IsMerge(right))
            {
                _graph.Insert(left, Direction.Down, br);
                _graph.Insert(right, Direction.Down, mg);
                _graph.InterConnect(br, Direction.Right, mg);
                _graph.Insert(br, Direction.Right, contact);
            }
            else if (LayoutNode.IsBranchTerminal(left) && LayoutNode.IsMergeTerminal(right))
            {
                _graph.Insert(left, Direction.Up, br);
                _graph.Insert(right, Direction.Up, mg);
                _graph.InterConnect(br, Direction.Right, mg);
                _graph.InterConnect(left, Direction.Right, contact);
                _graph.InterConnect(contact, Direction.Right, right);
                _graph.Insert(br, Direction.Right, orgSel);
            }
            else
            {
                _graph.Insert(left, Direction.Right, br);
                var bt = new BranchTerminalNode();
                _graph.Insert(br, Direction.Down, bt);
                _graph.Insert(orgSel, Direction.Right, mg);
                var mt = new MergeTerminalNode();
                _graph.Insert(mg, Direction.Down, mt);
                _graph.InterConnect(bt, Direction.Right, mt);
                _graph.Insert(bt, Direction.Right, contact);
            }

            ModelChanged?.Invoke(this, EventArgs.Empty);
            viewModel.Selected = contact;
            EditOperand(viewModel, contact);
        }

        private int WidthGetter(string s)
        {
            using (var g = _parent.CreateGraphics())
            {
                return (int)g.MeasureString(s, _parent.Font).Width + 1;
            }
        }
    }
}