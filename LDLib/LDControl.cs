﻿namespace LDLib
{
    public partial class LDControl : UserControl
    {
        private Graph _graph;
        private ViewModel _viewModel;
        private EditService _editService;
        private MonitorService _monitorService;

        public bool MonitorMode
        {
            set
            {
                _monitorService.SetMonMode(value);
            }
            get
            {
                return _monitorService.IsMonMode();
            }
        }

        public LDControl()
        {
            InitializeComponent();
            this.DoubleBuffered = true;

            _graph = EditService.CreateGraph();
            _editService = new(_graph, this);
            _monitorService = new();
            _monitorService.Changed += (_, _) => this.Invalidate();
            _viewModel = new(LayoutFactory.Create(_graph, this.Width, _monitorService));

            _editService.ModelChanged += _editService_ModelChanged;
            this.MouseDown += (_, e) => SelectNode(e.Location);
            this.MouseDoubleClick += (_, e) =>
            {
                SelectNode(e.Location);
                _editService.Edit(_viewModel);
            };
            this.Paint += (_, e) => DrawService.Draw(e.Graphics, _viewModel, _graph, this.Font, _monitorService);
            this.Resize += (_, _) => _viewModel.Layout = LayoutFactory.Create(_graph, this.Width, _monitorService);
            ConnectViewModelEvent();
        }

        private void _editService_ModelChanged(object? sender, EventArgs e)
        {
            ConnectViewModelEvent();
        }

        private void ConnectViewModelEvent()
        {
            _viewModel.Layout = LayoutFactory.Create(_graph, this.Width, _monitorService);
            _viewModel.Changed += (_, _) => this.Invalidate();
        }

        private void SelectNode(Point location)
        {
            foreach (var n in _viewModel.AllNodes)
            {
                if (!_viewModel.TryGetNodeRect(n, out var rect)) continue;
                if (!rect.Contains(location)) continue;
                _viewModel.Selected = n;
            }
        }


        protected override bool ProcessCmdKey(ref Message msg, Keys keyData)
        {
            if (TryGetMoveDirection(keyData))
            {
                return true;
            }
            else if (keyData == Keys.C)
            {
                _editService.InsertContact(_viewModel);
            }
            else if (keyData == Keys.F)
            {
                _viewModel.Selected = _editService.InsertFb(_viewModel.Selected);
            }
            else if (keyData == Keys.W)
            {
                _editService.InsertOrContact(_viewModel);
            }
            else if (keyData == Keys.O)
            {
                _editService.InsertCoil(_viewModel);
            }
            else if (keyData == Keys.Delete)
            {
                _editService.Delete(_viewModel);
            }else if (keyData == Keys.Enter)
            {
                _editService.Edit(_viewModel);
            }
            return true;
        }

        private bool TryGetMoveDirection(Keys keyData)
        {
            Direction direction;
            if (keyData == Keys.Right) direction = Direction.Right;
            else if (keyData == Keys.Left) direction = Direction.Left;
            else if (keyData == Keys.Up) direction = Direction.Up;
            else if (keyData == Keys.Down) direction = Direction.Down;
            else
            {
                direction = Direction.Up;
                return false;
            }

            if (_viewModel.Selected == null) return true;
            if (!_graph.TryGetNode(_viewModel.Selected, direction, out var next)) return true;
            _viewModel.Selected = next;
            return true;
        }
    }
}
