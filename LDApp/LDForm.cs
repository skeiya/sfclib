namespace LDApp
{
    public partial class LDForm : Form
    {
        public LDForm()
        {
            InitializeComponent();
        }

        private void monitorToolStripMenuItem_CheckedChanged(object sender, EventArgs e)
        {
            ldControl1.MonitorMode = monitorToolStripMenuItem.Checked;
        }
    }
}